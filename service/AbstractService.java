package de.mercury.backendapi;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
public class AbstractService<R> {

    private JpaRepository<R, UUID> repository;

    public List<R> getAll() {
        return repository.findAll();
    }

    public Optional<R> getById(UUID id) {
        return repository.findById(id);
    }

    public R put(R entity) {
        return repository.save(entity);
    }

    public List<R> putAll(Iterable<R> entities) {
        return repository.saveAll(entities);
    }

    public List<R> getAllByIds(Iterable<UUID> ids) {
        return repository.findAllById(ids);
    }
}
