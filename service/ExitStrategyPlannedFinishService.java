package package de.mercury.backendapi.templates.service.exit_strategy.elements;

import de.mercury.backendapi.AbstractService;
import de.mercury.backendapi.templates.model.exit_strategy.elements.ExitStrategyPlannedFinish;
import de.mercury.backendapi.templates.model.exit_strategy.elements.PlannedAdditionalInfoForOption;
import de.mercury.backendapi.templates.model.questionnaire.FilledOutQuestionnaire;
import de.mercury.backendapi.templates.model.questionnaire.QuestionnaireType;
import de.mercury.backendapi.templates.repository.exit_strategy.elements.ExitStrategyPlannedFinishRepository;
import de.mercury.backendapi.templates.service.questionnaire.FilledOutQuestionnaireService;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ExitStrategyPlannedFinishService extends AbstractService<ExitStrategyPlannedFinish> {

    private final PlannedAdditionalInfoForOptionService plannedAdditionalInfoForOptionService;
    private final FilledOutQuestionnaireService filledOutQuestionnaireService;
    private final ExitStrategyPlannedFinishRepository exitStrategyPlannedFinishRepository;

    public ExitStrategyPlannedFinishService(ExitStrategyPlannedFinishRepository repository, PlannedAdditionalInfoForOptionService plannedAdditionalInfoForOptionService, FilledOutQuestionnaireService filledOutQuestionnaireService, ExitStrategyPlannedFinishRepository exitStrategyPlannedFinishRepository) {
        super(repository);
        this.plannedAdditionalInfoForOptionService = plannedAdditionalInfoForOptionService;
        this.filledOutQuestionnaireService = filledOutQuestionnaireService;
        this.exitStrategyPlannedFinishRepository = exitStrategyPlannedFinishRepository;
    }

    public ExitStrategyPlannedFinish createExitStrategyPlannedFinish(QuestionnaireType actionEvaluationType) {
        FilledOutQuestionnaire actionEvaluationFilledOutQuestionnaire = filledOutQuestionnaireService.createViaQuestionnaireType(actionEvaluationType);
        ExitStrategyPlannedFinish exitStrategyPlannedFinish = new ExitStrategyPlannedFinish(actionEvaluationFilledOutQuestionnaire);
        List<PlannedAdditionalInfoForOption> plannedAdditionalInfoForOptions = new ArrayList<>();

        actionEvaluationFilledOutQuestionnaire.getQuestionnaire().getQuestions().forEach(question -> {
            val plannedAdditionalInfoForOption = new PlannedAdditionalInfoForOption(question);

            plannedAdditionalInfoForOption.setExitStrategyPlannedFinish(exitStrategyPlannedFinish);

            plannedAdditionalInfoForOptions.add(plannedAdditionalInfoForOption);
        });

        exitStrategyPlannedFinish.setPlannedAdditionalInfoForOptions(plannedAdditionalInfoForOptions);

        return this.put(exitStrategyPlannedFinish);
    }

    public ExitStrategyPlannedFinish updateExitStrategyPlannedFinish(UUID id, ExitStrategyPlannedFinish updatedExitStrategyPlannedFinish) {
        Optional<ExitStrategyPlannedFinish> exitStrategyPlannedFinishOptional = this.getById(id);

        if (exitStrategyPlannedFinishOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unplanned Finish entries not found");
        }
        ExitStrategyPlannedFinish exitStrategyPlannedFinish = exitStrategyPlannedFinishOptional.get();
        plannedAdditionalInfoForOptionService.updatePlannedAdditionalInfoForOptions(updatedExitStrategyPlannedFinish.getPlannedAdditionalInfoForOptions());
        exitStrategyPlannedFinish.setPlannedAdditionalInfoForOptions(this.exitStrategyPlannedFinishRepository.findPlannedAdditionalInfoForOptionsById(id));
        exitStrategyPlannedFinish.setIsFilledOut(true);
        return this.put(exitStrategyPlannedFinish);
    }
}
