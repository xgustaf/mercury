package de.mercury.backendapi.templates.service.provider;

import de.mercury.backendapi.templates.dto.provider.EditProviderDto;
import de.mercury.backendapi.templates.model.provider.Provider;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProviderService {
    Optional<Provider> findById(UUID id);

    Provider getById(UUID id);

    List<Provider> getAll();

    Page<Provider> getProviderPage(PageRequest pageRequest);

    boolean existsByName(String name);

    boolean existsByCompanyRegistrationNumber(String companyRegistrationNumber);

    boolean existsByLegalEntityIdentifier(String legalEntityIdentifier);

    Provider createNewProvider(EditProviderDto providerDto);

    void deleteProvider(Provider provider);

    int getReferenceCount(Provider provider);

    Provider updateProvider(Provider provider, EditProviderDto providerDto);

    @AllArgsConstructor
    @Getter
    class CountryNotFound extends RuntimeException {
        private final UUID wantedId;
        private final String wantedField;
    }

    @AllArgsConstructor
    @Getter
    class FieldConflictsWithExisting extends RuntimeException {
        private final String field;
    }

    class ProviderStillInUse extends RuntimeException {
    }
}
