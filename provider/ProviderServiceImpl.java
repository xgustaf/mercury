package de.mercury.backendapi.templates.service.provider;

import de.mercury.backendapi.templates.dto.provider.EditProviderDto;
import de.mercury.backendapi.templates.model.common.Country;
import de.mercury.backendapi.templates.model.provider.Provider;
import de.mercury.backendapi.templates.repository.provider.ProviderRepository;
import de.mercury.backendapi.templates.service.common.CountryService;
import de.mercury.backendapi.templates.service.reference.ReferenceCountService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ProviderServiceImpl implements ProviderService {
    private final ProviderRepository providerRepository;
    private final CountryService countryService;
    private final ReferenceCountService referenceCountService;
    private final ModelMapper mapper;

    @Override
    public Optional<Provider> findById(UUID id) {
        return providerRepository.findById(id);
    }

    @Override
    public Provider getById(UUID id) {
        return findById(id).orElseThrow(() -> new ProviderNotFoundException(id));
    }

    @Override
    public List<Provider> getAll() {
        return providerRepository.findAll();
    }

    @Override
    public Page<Provider> getProviderPage(PageRequest pageRequest) {
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, "name"));

        return providerRepository.findAll(pageRequest.withSort(sort));
    }

    @Override
    public boolean existsByName(String name) {
        return providerRepository.existsByNameTrimIgnoreCase(name);
    }

    @Override
    public boolean existsByCompanyRegistrationNumber(String companyRegistrationNumber) {
        return providerRepository.existsByCompanyRegistrationNumberTrimIgnoreCase(companyRegistrationNumber);
    }

    @Override
    public boolean existsByLegalEntityIdentifier(String legalEntityIdentifier) {
        return providerRepository.existsByLegalEntityIdentifierTrimIgnoreCase(legalEntityIdentifier);
    }

    @Override
    @Transactional
    public Provider createNewProvider(EditProviderDto providerDto) {
        checkForFieldConflicts(providerDto);

        Provider newProvider = mapper.map(providerDto, Provider.class);
        setCountriesFromDto(providerDto, newProvider);

        return providerRepository.save(newProvider);
    }

    private void checkForFieldConflicts(EditProviderDto providerDto) {
        if (providerRepository.existsByNameTrimIgnoreCase(providerDto.getName())) {
            throw new FieldConflictsWithExisting("name");
        }
        if (providerRepository.existsByCompanyRegistrationNumberTrimIgnoreCase(providerDto.getCompanyRegistrationNumber())) {
            throw new FieldConflictsWithExisting("companyRegistrationNumber");
        }
        if (StringUtils.hasText(providerDto.getLegalEntityIdentifier())
                && providerRepository.existsByLegalEntityIdentifierTrimIgnoreCase(providerDto.getLegalEntityIdentifier())) {
            throw new FieldConflictsWithExisting("legalEntityIdentifier");
        }
    }

    @Override
    @Transactional
    public void deleteProvider(Provider provider) {
        if (getReferenceCount(provider) > 0) {
            throw new ProviderStillInUse();
        }
        providerRepository.delete(provider);
    }

    @Override
    public int getReferenceCount(Provider provider) {
        return referenceCountService.getNumberOfEntitiesLinkedToProvider(provider);
    }

    @Override
    @Transactional
    public Provider updateProvider(Provider provider, EditProviderDto providerDto) {
        checkForFieldConflicts(providerDto, provider.getId());

        mapper.map(providerDto, provider);
        setCountriesFromDto(providerDto, provider);

        return providerRepository.save(provider);
    }

    private void checkForFieldConflicts(EditProviderDto providerDto, UUID idToIgnore) {
        if (providerRepository.existsByNameTrimIgnoreCase(providerDto.getName(), idToIgnore)) {
            throw new FieldConflictsWithExisting("name");
        }
        if (providerRepository.existsByCompanyRegistrationNumberTrimIgnoreCase(providerDto.getCompanyRegistrationNumber(), idToIgnore)) {
            throw new FieldConflictsWithExisting("companyRegistrationNumber");
        }
        if (StringUtils.hasText(providerDto.getLegalEntityIdentifier())
                && providerRepository.existsByLegalEntityIdentifierTrimIgnoreCase(providerDto.getLegalEntityIdentifier(), idToIgnore)) {
            throw new FieldConflictsWithExisting("legalEntityIdentifier");
        }
    }

    private void setCountriesFromDto(EditProviderDto providerDto, Provider provider) {
        Country country = countryService.getById(providerDto.getCountryId())
                .orElseThrow(() -> new CountryNotFound(providerDto.getCountryId(), "countryId"));
        provider.setCountry(country);

        if (providerDto.getParentCountryId() != null) {
            Country parentCountry = countryService.getById(providerDto.getParentCountryId())
                    .orElseThrow(() -> new CountryNotFound(providerDto.getParentCountryId(), "parentCountryId"));

            provider.setParentCountry(parentCountry);
        }
    }

    public static class ProviderNotFoundException extends RuntimeException {
        public ProviderNotFoundException(UUID id) {
            super(format("Provider with ID %s was not found.", id));
        }
    }
}
